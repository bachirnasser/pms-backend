﻿function showInformativeMessage(message) {
    var $informativeMessage = $("<div></div>").html(message);
    $informativeMessage.dialog({ modal: true, title: 'Notice' });
}

function showLoader(message) {
    var $informativeMessage = $('<div id="Loader"></div>').html(message);
    $informativeMessage.dialog({ modal: true, open: function (event, ui) { $(".ui-dialog-titlebar").hide(); $(".ui-dialog-titlebar-close").hide(); } });
}

function hideLoader() {
    $('#Loader').dialog('close');
    $('#Loader').remove();
    return;
}


function hideInformativeMessage() {
    return;
}

function showTimePeriodPicker() {
    var $informativeMessage = $("#timePeriodsContainer");
    $informativeMessage.show();
}

function hideTimePeriodPicker() {
    var $informativeMessage = $("#timePeriodsContainer");
    $informativeMessage.hide();
}


function getQueryStringParameters(url) {
    var parameters = new Object();
    var separatorIndex = url.indexOf('?');

    if (separatorIndex > 0) {
        var queryString = url.substring(url.indexOf('?') + 1);
        var pairs = queryString.split('&');

        $.each(pairs, function (index, pair) {
            parameters[pair.split('=')[0]] = pair.split('=')[1];
        });
    }

    return parameters;
}

function _deprecated_showInformativeMessage(message) {
    var $informativeMessage = $("#globalInformativeMessage");
    $informativeMessage.text(message);
    $informativeMessage.fadeIn(200);
    setInterval(hideInformativeMessage, 10000);
}

function _deprecated_hideInformativeMessage() {
    var $informativeMessage = $("#globalInformativeMessage");
    $informativeMessage.fadeOut();
}
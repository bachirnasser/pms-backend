//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMS.DataInterface.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class balda
    {
        public string balda_code { get; set; }
        public string balda_desc { get; set; }
        public string kadaa { get; set; }
        public string kadaa_code { get; set; }
        public string MOHAFAZA { get; set; }
        public string balda1 { get; set; }
        public string balda_add { get; set; }
        public string daira_code { get; set; }
    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PMS.WebApplication.Startup))]
namespace PMS.WebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS.DataInterface.EDMX;

namespace PMS.WebApplication.Controllers
{
    public class partiesController : Controller
    {
        private Beirut1_WBEntities db = new Beirut1_WBEntities();

        // GET: parties
        public ActionResult Index()
        {
            return View(db.parties.ToList());
        }

        // GET: parties/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            party party = db.parties.Find(id);
            if (party == null)
            {
                return HttpNotFound();
            }
            return View(party);
        }

        // GET: parties/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: parties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "party_desc,party_group")] party party)
        {
            if (ModelState.IsValid)
            {
                if(party.party_desc == null)
                {
                    party.party_desc = party.party_group;
                }

                db.parties.Add(party);
                db.SaveChanges();
                return View("~/Views/UserManagement/Index.cshtml");

                //return RedirectToAction("Index");
            }

            return View(party);
        }

        // GET: parties/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            party party = db.parties.Find(id);
            if (party == null)
            {
                return HttpNotFound();
            }
            return View(party);
        }

        // POST: parties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "party_desc,party_group")] party party)
        {
            if (ModelState.IsValid)
            {
                if (party.party_desc == null)
                {
                    party.party_desc = party.party_group;
                }

                db.Entry(party).State = EntityState.Modified;
                db.SaveChanges();
                return View("~/Views/UserManagement/Index.cshtml");

                //return RedirectToAction("Index");
            }
            return View(party);
        }

        // GET: parties/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            party party = db.parties.Find(id);
            if (party == null)
            {
                return HttpNotFound();
            }
            return View(party);
        }

        // POST: parties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            party party = db.parties.Find(id);
            db.parties.Remove(party);
            db.SaveChanges();
            return View("~/Views/UserManagement/Index.cshtml");

            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

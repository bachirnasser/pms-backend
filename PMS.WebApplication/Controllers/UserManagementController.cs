﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PMS.WebApplication.Globals;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;

namespace PMS.WebApplication.Controllers
{
    [Authorize]
    public class UserManagementController : Controller
    {
        // GET: UserManagement
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult Index(string viewName = "CreateUserStepOne")
        {
            ViewBag.ViewName = viewName;
            return View();
        }

        async public Task<JsonResult> CreateUserStepOne()
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/RenderRazorViewToString/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("viewName={0}", "CreateUserStepOne");
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
            //return result;
        }

        async public Task<string> getGroupsAndUsers(string user, string group)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getGroupsAndUsers/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("user={0}", Url.Encode(user));
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("group={0}", group);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
            //return result;
        }



        async public Task<string> gettotalnumberofrows(string HashCode = null)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/gettotalnumberofrows/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("HashCode={0}", Url.Encode(HashCode));
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
            //return result;
        }

        async public Task<JsonResult> RegisterUser(string UserName, string Password, string FirstName = null, string LastName = null,
            string PhoneNumber = null, string Address = null, string JobDescription = null, string UserType = null, string Kadaa = null,
            string Mouhafaza = null, string Balda = null, string Notes = null, string Role = null, string Dashboard = null)
        {

            string jResult = "";
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/RegisterUser/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("UserName={0}", UserName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Password={0}", Password);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("FirstName={0}", FirstName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("LastName={0}", LastName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("PhoneNumber={0}", PhoneNumber);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Address={0}", Address);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("JobDescription={0}", JobDescription);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("UserType={0}", UserType);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Kadaa={0}", Kadaa);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Mouhafaza={0}", Mouhafaza);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Balda={0}", Balda);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Notes={0}", Notes);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Role={0}", Role);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Dashboard={0}", Dashboard);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("LoggedInUser={0}", User.Identity.GetUserName());

                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
                jResult = JsonConvert.SerializeObject(result);
            }
            return Json(jResult, JsonRequestBehavior.AllowGet);
            //return result;
        }
        [HttpPost]
        async public Task<string> getIndividuals(string hash = "1=1", string pagination = "")
        {
            //Edit By Hussein
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                //pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                //pmClient.DefaultRequestHeaders.Accept.Clear();
                //pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //StringBuilder stringBuilder = new StringBuilder();
                //stringBuilder.Append("UserManagement/getIndividuals/");
                ////stringBuilder.Append("&");
                //stringBuilder.AppendFormat("?hash={0}", Url.Encode(hash));

                //HttpResponseMessage response = await pmClient.PostAsync(stringBuilder.ToString());
                //response.EnsureSuccessStatusCode();
                //if (response.IsSuccessStatusCode)
                //{
                //    result = await response.Content.ReadAsStringAsync();
                //}

                var pairs = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("hash", hash),
                new KeyValuePair<string, string>("pagination",pagination)
            };

                var content = new FormUrlEncodedContent(pairs);

                var client = new HttpClient { BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL) };

                // call sync
                var response = await client.PostAsync("/UserManagement/getIndividuals", content).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }

            }
            return result;
        }

        async public Task<string> getJobDescription(string id, string SearchTerm)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getJobDescription/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("id={0}", id);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("SearchTerm={0}", SearchTerm);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> getHash(string hash)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getHash/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("hash={0}", Url.Encode(hash));
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> OwnerFieldAssign(string userName, string ownerColumn)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/OwnerFieldAssign/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("userName={0}", userName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("ownerColumn={0}", ownerColumn);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }






        async public Task<JsonResult> DeleteGroup(string groupname)
        {
            string result = "";

            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/DeleteGroup/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("groupname={0}", groupname);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);

        }



        async public Task<JsonResult> DeleteUser(string UserName)
        {
            string result = "";

            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/DeleteUser/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("UserName={0}", UserName);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        async public Task<JsonResult> DeleteDataOwner(string username, string group, string fields)
        {
            string result = "";

            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/DeleteDataOwner/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("username={0}", username);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("group={0}", group);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("fields={0}", fields);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);

        }








        async public Task<JsonResult> DeleteAction(string UserName, string ActionName, string From, string Description, string ForwardToUser, string Notify)
        {
            string result = "";

            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/DeleteAction/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("UserName={0}", UserName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("ActionName={0}", ActionName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("From={0}", From);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Description={0}", Description);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("ForwardToUser={0}", ForwardToUser);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Notify={0}", Notify);

                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        async public Task<string> getFromIndividuals(string selectVariable = null, string whereVariable = null, string resultVariable = null)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getFromIndividuals/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("selectVariable={0}", Url.Encode(selectVariable));
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("whereVariable={0}", Url.Encode(whereVariable));
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("resultVariable={0}", Url.Encode(resultVariable));
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> GETNeighborhoods(string balda = null)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/GETNeighborhoods/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("balda={0}", Url.Encode(balda));
              
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<JsonResult> EditGroup(string oldgroupname, string newgroupname, string newgroupdesc)
        {
            string result = "";

            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/EditGroup/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("oldgroupname={0}", oldgroupname);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("newgroupname={0}", newgroupname);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("newgroupdesc={0}", newgroupdesc);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);

        }


        async public Task<string> getFromIndividualsCombined(string Kadaa = null, string balda = null, string tayfe = null)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getFromIndividualsCombined/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("Kadaa={0}", Url.Encode(Kadaa));
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("balda={0}", Url.Encode(balda));
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("tayfe={0}", Url.Encode(tayfe));
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> getUsers()
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getUsers/");
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> getDataOwners()
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getDataOwners/");
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }


        //michelaboughanem
        async public Task<string> GetUserRole(string Username)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/GetUserRole/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("Username={0}", Username);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }


        async public Task<string> getServices(string service = null)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getServices/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("service={0}", service);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }



        async public Task<string> getRoles()
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getRoles/");
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> getMohafaza()
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getMohafaza/");
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> getKadaa()
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getKadaa/");
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        //async public Task<string> getBalda()
        //{
        //    string result = "";
        //    using (HttpClient pmClient = new HttpClient())
        //    {
        //        pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
        //        pmClient.DefaultRequestHeaders.Accept.Clear();
        //        pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        StringBuilder stringBuilder = new StringBuilder();
        //        stringBuilder.Append("UserManagement/getBalda/");
        //        HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            result = await response.Content.ReadAsStringAsync();
        //        }
        //    }
        //    return result;
        //}


        async public Task<string> getBalda(string id, string SearchTerm)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getBalda/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("id={0}", id);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("SearchTerm={0}", SearchTerm);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> getServicesJSON(string service = null)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getServicesJSON/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("service={0}", service);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> CreateGroup(string createdBy, string GroupName, string Hash, string assignedToUser, string groupDescription)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/CreateGroup/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("createdBy={0}", createdBy);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("GroupName={0}", GroupName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Hash={0}", Hash);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("assignedToUser={0}", assignedToUser);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("groupDescription={0}", groupDescription);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }



        async public Task<string> setActionsForUser(string UserName, string ActionName, string ForwardToUser, string From, string Notify, string Description = null, string UserRegion = null, string VisitType = null)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/setActionsForUser/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("UserName={0}", UserName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("ActionName={0}", ActionName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("ForwardToUser={0}", ForwardToUser);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("From={0}", From);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Notify={0}", Notify);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Description={0}", Description);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("UserRegion={0}", UserRegion);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("VisitType={0}", VisitType);

                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }


        async public Task<string> updateActionsForUser(string UserName, string ActionName, string ForwardToUser, string FromUser = null, string VisitType = null, string Description = "")
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/updateActionsForUser/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("UserName={0}", Url.Encode(UserName));
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("ActionName={0}", Url.Encode(ActionName));
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("ForwardToUser={0}", Url.Encode(ForwardToUser));
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("FromUser={0}", FromUser);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("VisitType={0}", VisitType);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Description={0}", Description);

                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        //async public Task<string> setActionsForUser(string UserName, string ActionName, string ForwardToUser, string From, string Notify, string Description = null, string UserRegion = null)
        //{
        //    string result = "";
        //    using (HttpClient pmClient = new HttpClient())
        //    {
        //        pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
        //        pmClient.DefaultRequestHeaders.Accept.Clear();
        //        pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        StringBuilder stringBuilder = new StringBuilder();
        //        stringBuilder.Append("UserManagement/setActionsForUser/");
        //        stringBuilder.Append("?");
        //        stringBuilder.AppendFormat("UserName={0}", UserName);
        //        stringBuilder.Append("&");
        //        stringBuilder.AppendFormat("ActionName={0}", ActionName);
        //        stringBuilder.Append("&");
        //        stringBuilder.AppendFormat("ForwardToUser={0}", ForwardToUser);
        //        stringBuilder.Append("&");
        //        stringBuilder.AppendFormat("From={0}", From);
        //        stringBuilder.Append("&");
        //        stringBuilder.AppendFormat("Notify={0}", Notify);
        //        stringBuilder.Append("&");
        //        stringBuilder.AppendFormat("Description={0}", Description);
        //        stringBuilder.Append("&");
        //        stringBuilder.AppendFormat("UserRegion={0}", UserRegion);
        //        HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            result = await response.Content.ReadAsStringAsync();
        //        }
        //    }
        //    return result;
        //}

        async public Task<string> getActions()
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getActions/");
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> AssignIndividualsToUser(string UserName, string Hash)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/AssignIndividualsToUser/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("UserName={0}", UserName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Hash={0}", Hash);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> getActionsByUsers(string UserName = null)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getActionsByUsers/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("UserName={0}", UserName);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }

        async public Task<string> getActionsByUsersForward(string UserName = null)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/getActionsByUsersForward/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("UserName={0}", UserName);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            return result;
        }
        //[HttpGet]
        async public Task<JsonResult> GetView(string ViewName)
        {
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UserManagement/RenderRazorViewToString/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("viewName={0}", ViewName);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
            //return result;
        }
    }
}
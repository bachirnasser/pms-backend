﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS.DataInterface.EDMX;
using System.Data.SqlClient;

namespace PMS.WebApplication.Controllers
{
    public class etatsController : Controller
    {
        private Beirut1_WBEntities db = new Beirut1_WBEntities();

        // GET: etats
        public ActionResult Index()
        {
            return View(db.etats.ToList());
        }

        // GET: etats/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            etat etat = db.etats.Find(id);
            if (etat == null)
            {
                return HttpNotFound();
            }
            return View(etat);
        }

        // GET: etats/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: etats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "etat_desc,ID")] etat etat)
        {
            //if (ModelState.IsValid)
            //{
            //    db.etats.Add(etat);
            //    db.SaveChanges();
            //    return View("~/Views/UserManagement/Index.cshtml");

            //    //return RedirectToAction("Index");
            //}



            var MaxID = db.etats.ToList<etat>().Max(x => x.ID + 1);


            using (var conn = new SqlConnection())
            {
                conn.ConnectionString =
                        "Data Source=server1.dev.wbintl.co;" +
                        "Initial Catalog=BeirutUI;" +
                        "User id=PMProduction;" +
                        "Password=PM$P@ss01;";

                //conn.ConnectionString =
                //    "Data Source=74.208.165.40;" +
                //    "Initial Catalog=BeirutUI;" +
                //    "User id=PMProduction;" +
                //    "Password=PM$P@ss01;";

                conn.Open();

                var command =
                    new SqlCommand("SET IDENTITY_INSERT [BeirutUI].[dbo].[etat] ON; INSERT INTO [BeirutUI].[dbo].[etat] ( [ID],[etat_desc]) VALUES(" + MaxID + ", N'" + etat.etat_desc + "'); SET IDENTITY_INSERT [BeirutUI].[dbo].[etat] OFF;", conn);

                command.ExecuteNonQuery();

                conn.Close();
                return View("~/Views/UserManagement/Index.cshtml");

            }

            return View(etat);
        }

        // GET: etats/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            etat etat = db.etats.Find(id);
            if (etat == null)
            {
                return HttpNotFound();
            }
            return View(etat);
        }

        // POST: etats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "etat_desc,ID")] etat etat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(etat).State = EntityState.Modified;
                db.SaveChanges();
                return View("~/Views/UserManagement/Index.cshtml");

                //return RedirectToAction("Index");
            }
            return View(etat);
        }

        // GET: etats/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            etat etat = db.etats.Find(id);
            if (etat == null)
            {
                return HttpNotFound();
            }
            return View(etat);
        }

        // POST: etats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            etat etat = db.etats.Find(id);
            db.etats.Remove(etat);
            db.SaveChanges();
            return View("~/Views/UserManagement/Index.cshtml");

            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS.DataInterface.EDMX;
using System.Data.SqlClient;

namespace PMS.WebApplication.Controllers
{
    public class EvaluationsController : Controller
    {
        private Beirut1_WBEntities db = new Beirut1_WBEntities();

        // GET: Evaluations
        public ActionResult Index()
        {
            return View(db.Evaluations.ToList());
        }

        // GET: Evaluations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evaluation evaluation = db.Evaluations.Find(id);
            if (evaluation == null)
            {
                return HttpNotFound();
            }
            return View(evaluation);
        }

        // GET: Evaluations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Evaluations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "evaluation_desc")] Evaluation evaluation)
        {
            if (ModelState.IsValid)
            {
                evaluation.daira_code = "1";

                var MaxEvaluationCode = db.Evaluations.ToList<Evaluation>().Max(x => x.evaluation_code +14) ;
                //evaluation.evaluation_code = MaxEvaluationCode;

                //db.Evaluations.Add(evaluation);

                using (var conn = new SqlConnection())
                {
                    conn.ConnectionString =
                        "Data Source=server1.dev.wbintl.co;" +
                        "Initial Catalog=BeirutUI;" +
                        "User id=PMProduction;" +
                        "Password=PM$P@ss01;";

                    //conn.ConnectionString =
                    //    "Data Source=74.208.165.40;" +
                    //    "Initial Catalog=BeirutUI;" +
                    //    "User id=PMProduction;" +
                    //    "Password=PM$P@ss01;";

                    conn.Open();

                    var command =
                        new SqlCommand("SET IDENTITY_INSERT [BeirutUI].[dbo].[Evaluation] ON; INSERT INTO [BeirutUI].[dbo].[Evaluation] ( [daira_code],[evaluation_code],[evaluation_desc]) VALUES('1', " + MaxEvaluationCode + ", N'" + evaluation.evaluation_desc + "'); SET IDENTITY_INSERT [BeirutUI].[dbo].[Evaluation] OFF;", conn);

                    command.ExecuteNonQuery();

                    conn.Close();

                    return View("~/Views/UserManagement/Index.cshtml");
                }
            }
            //db.SaveChanges();
                return View("~/Views/UserManagement/Index.cshtml");

                //return RedirectToAction("Index");

            return View(evaluation);
        }

        // GET: Evaluations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evaluation evaluation = db.Evaluations.Find(id);
            if (evaluation == null)
            {
                return HttpNotFound();
            }
            return View(evaluation);
        }

        // POST: Evaluations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "daira_code,evaluation_code,evaluation_desc")] Evaluation evaluation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(evaluation).State = EntityState.Modified;
                db.SaveChanges();
                return View("~/Views/UserManagement/Index.cshtml");

                //return RedirectToAction("Index");
            }
            return View(evaluation);
        }

        // GET: Evaluations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evaluation evaluation = db.Evaluations.Find(id);
            if (evaluation == null)
            {
                return HttpNotFound();
            }
            return View(evaluation);
        }

        // POST: Evaluations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Evaluation evaluation = db.Evaluations.Find(id);
            db.Evaluations.Remove(evaluation);
            db.SaveChanges();
            return View("~/Views/UserManagement/Index.cshtml");

            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PMS.WebApplication.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            //bool HandShake = await HandShakeCheck();

            //if (!HandShake)
            //{
            //    return RedirectToAction("Login", "Account");
            //}

            return RedirectToAction("Index", "UserManagement");
            //View("Index", "UserManagement");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            return sMacAddress;
        }

        public string GetPublicIPAddress()
        {
            string externalip = new WebClient().DownloadString("http://icanhazip.com");

            return externalip;
        }

        string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        async Task<bool> HandShakeCheck()
        {
            bool resultOutput;

            string IPAddress = GetPublicIPAddress();

            IPAddress = IPAddress.Replace("\n", String.Empty);

            string MacAddress = GetMACAddress();

            string DateNow = DateTime.UtcNow.Date.ToString("yyyy-MM-dd hh:mm tt");

            string Combination = IPAddress + MacAddress + DateNow;

            string resultLocal = CalculateMD5Hash(Combination);

            string resultHandShake = string.Empty;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.HANDSHAKE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("api/HandShake/InitializeHand/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("stringOne={0}", IPAddress);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("stringTwo={0}", MacAddress);

                HttpResponseMessage response = await client.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultHandShake = await response.Content.ReadAsStringAsync();
                }
            }

            resultHandShake = RemoveSpecialCharacters(resultHandShake);

            resultOutput = resultLocal == resultHandShake ? true : false;

            return resultOutput;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}
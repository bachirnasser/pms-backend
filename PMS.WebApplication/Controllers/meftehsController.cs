﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS.DataInterface.EDMX;
using System.Data.SqlClient;

namespace PMS.DataInterface.Controllers
{
    public class meftehsController : Controller
    {
        private Beirut1_WBEntities db = new Beirut1_WBEntities();

        // GET: meftehs
        public ActionResult Index()
        {
            return View(db.meftehs.ToList());
        }

        // GET: meftehs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mefteh mefteh = db.meftehs.Find(id);
            if (mefteh == null)
            {
                return HttpNotFound();
            }
            return View(mefteh);
        }

        // GET: meftehs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: meftehs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "mefteh_code,mefteh_name,meftef_phone")] mefteh mefteh)
        {
            mefteh.mefteh_code = 1;

            var MaxMeftehCode = db.meftehs.ToList<mefteh>().Max(x => x.mefteh_code + 1);


            using (var conn = new SqlConnection())
            {
                conn.ConnectionString =
                        "Data Source=server1.dev.wbintl.co;" +
                        "Initial Catalog=BeirutUI;" +
                        "User id=PMProduction;" +
                        "Password=PM$P@ss01;";

                //conn.ConnectionString =
                //    "Data Source=74.208.165.40;" +
                //    "Initial Catalog=BeirutUI;" +
                //    "User id=PMProduction;" +
                //    "Password=PM$P@ss01;";

                conn.Open();

                var command =
                    new SqlCommand("SET IDENTITY_INSERT [BeirutUI].[dbo].[mefteh] ON; INSERT INTO [BeirutUI].[dbo].[mefteh] ( [mefteh_code],[mefteh_name],[meftef_phone]) VALUES(" + MaxMeftehCode + ", N'" + mefteh.mefteh_name + "', N'" + mefteh.meftef_phone + "'); SET IDENTITY_INSERT [BeirutUI].[dbo].[mefteh] OFF;", conn);

                command.ExecuteNonQuery();

                conn.Close();

                return View("~/Views/UserManagement/Index.cshtml");
            }
            //if (ModelState.IsValid)
            //{
            //    db.meftehs.Add(mefteh);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            return View(mefteh);
        }

        // GET: meftehs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mefteh mefteh = db.meftehs.Find(id);
            if (mefteh == null)
            {
                return HttpNotFound();
            }
            return View(mefteh);
        }

        // POST: meftehs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "mefteh_code,mefteh_name,meftef_phone")] mefteh mefteh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mefteh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mefteh);
        }

        // GET: meftehs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mefteh mefteh = db.meftehs.Find(id);
            if (mefteh == null)
            {
                return HttpNotFound();
            }
            return View(mefteh);
        }

        // POST: meftehs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            mefteh mefteh = db.meftehs.Find(id);
            db.meftehs.Remove(mefteh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

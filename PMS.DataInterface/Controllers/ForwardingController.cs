﻿using Newtonsoft.Json;
using PMS.DataInterface.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PMS.DataInterface.Controllers
{
    public class ForwardingController : Controller
    {
        private Data.WBI_PM_DEVEntitiesDataContext db = new Data.WBI_PM_DEVEntitiesDataContext();

        // GET: Forwarding
        public ActionResult Index()
        {
            ViewData["Users"] = db.AspNetUsers.ToList();
            ViewData["Actions"] = db.Actions.ToList();

            return View();
        }

        public ActionResult getUsers(string id)
        {
            if (string.IsNullOrEmpty(id))
                return Json(string.Empty);

            List<AspNetUser> users = db.AspNetUsers.Where(user => user.UserName.Contains(id)).ToList<AspNetUser>();

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult setupActivity(string configuration)
        {
            if(string.IsNullOrEmpty(configuration))
                return Json(string.Empty);

            List<Activity> activities;
            try
            {
                activities = JsonConvert.DeserializeObject<List<Activity>>(configuration);
            }
            catch
            {
                return Json(string.Empty);
            }

            db.Activities.InsertAllOnSubmit(activities);

            db.SubmitChanges();

            return Json(activities, JsonRequestBehavior.DenyGet);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS.DataInterface.EDMX;

namespace PMS.DataInterface.Controllers
{
    public class etatsController : Controller
    {
        private Beirut1_WBEntities db = new Beirut1_WBEntities();

        // GET: etats
        public ActionResult Index()
        {
            return View(db.etats.ToList());
        }

        // GET: etats/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            etat etat = db.etats.Find(id);
            if (etat == null)
            {
                return HttpNotFound();
            }
            return View(etat);
        }

        // GET: etats/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: etats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "etat_desc,ID")] etat etat)
        {
            if (ModelState.IsValid)
            {
                db.etats.Add(etat);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(etat);
        }

        // GET: etats/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            etat etat = db.etats.Find(id);
            if (etat == null)
            {
                return HttpNotFound();
            }
            return View(etat);
        }

        // POST: etats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "etat_desc,ID")] etat etat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(etat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(etat);
        }

        // GET: etats/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            etat etat = db.etats.Find(id);
            if (etat == null)
            {
                return HttpNotFound();
            }
            return View(etat);
        }

        // POST: etats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            etat etat = db.etats.Find(id);
            db.etats.Remove(etat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

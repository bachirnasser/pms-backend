﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PMS.DataInterface.EDMX;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using PMS.DataInterface.Globals;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PMS.DataInterface.Models;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;

namespace PMS.DataInterface.Controllers
{

    public class UserManagementController : Controller
    {
        private RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
        private ApplicationUserManager _userManager;

        // GET: UserManagement
        public ActionResult Index()
        {
            return View();
        }

        // GET: Create User Step One
        [HttpGet]
        public ActionResult CreateUserStepOne()
        {
            List<string> resultRoles = new List<string>();
            List<string> resultMohafaza = new List<string>();
            List<string> resultKadaa = new List<string>();
            List<string> resultBalda = new List<string>();
            ViewBag.Services = getServices(null);

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                //Get Roles,Mohafaza,Kadaa,Balda And Store Them In ViewData -- Values Are Passed To The View
                resultRoles = PMSPEntities.GetRole(null).ToList<string>();
                ViewData["Roles"] = resultRoles.Select(tier => new SelectListItem { Text = tier.Trim(), Value = tier.Trim() }).ToList();

                resultMohafaza = PMSPEntities.GetMohafaza(null).ToList<string>();
                ViewData["Mohafaza"] = resultMohafaza.Select(tier => new SelectListItem { Text = tier.Trim(), Value = tier.Trim() }).ToList();

                resultKadaa = PMSPEntities.GetKadaa(null).ToList<string>();
                ViewData["Kadaa"] = resultKadaa.Select(tier => new SelectListItem { Text = tier.Trim(), Value = tier.Trim() }).ToList();

                resultBalda = PMSPEntities.GETBalda(null).ToList<string>();
                ViewData["Balda"] = resultBalda.Select(tier => new SelectListItem { Text = tier.Trim(), Value = tier.Trim() }).ToList();
            }

            var directorRole = RoleManager.FindByName("Director").Id;
            var managerRole = RoleManager.FindByName("Manager").Id;

            //var Directors = UserManager.Users.Where(u => u.Roles.Any(r => r.RoleId == directorRole)).ToList();
            //var Managers = UserManager.Users.Where(u => u.Roles.Any(r => r.RoleId == managerRole)).ToList();
            //List<IdentityUser> allManagerUsers = new List<IdentityUser>();
            //allManagerUsers.AddRange(Directors);
            //allManagerUsers.AddRange(Managers);
            //List<SelectListItem> ActionUsers = allManagerUsers.Select(x => new SelectListItem { Text = x.UserName, Value = x.Id }).ToList();
            //ViewData["ActionUsers"] = ActionUsers;

            var users = UserManager.Users.ToList();

            List<SelectListItem> AllUsers = users.Select(x => new SelectListItem { Text = x.UserName, Value = x.Id }).ToList();
            ViewData["AllUsers"] = AllUsers;



            return View();
        }

        // GET: Create User Step Two
        [HttpGet]
        public ActionResult CreateUserStepTwo()
        {
            var directorRole = RoleManager.FindByName("Director").Id;
            var managerRole = RoleManager.FindByName("Manager").Id;

            var Directors = UserManager.Users.Where(u => u.Roles.Any(r => r.RoleId == directorRole)).ToList();
            var Managers = UserManager.Users.Where(u => u.Roles.Any(r => r.RoleId == managerRole)).ToList();
            List<IdentityUser> allManagerUsers = new List<IdentityUser>();
            allManagerUsers.AddRange(Directors);
            allManagerUsers.AddRange(Managers);
            List<SelectListItem> ActionUsers = allManagerUsers.Select(x => new SelectListItem { Text = x.UserName, Value = x.Id }).ToList();
            ViewData["ActionUsers"] = ActionUsers;

            var users = UserManager.Users.ToList();

            List<SelectListItem> AllUsers = users.Select(x => new SelectListItem { Text = x.UserName, Value = x.Id }).ToList();
            ViewData["AllUsers"] = AllUsers;
            return View();
        }

        // GET: Create User Step Three
        [HttpGet]
        public ActionResult CreateUserStepThree()
        {
            return View();
        }

        [HttpGet]
        public JsonResult getJobDescription(string id, string SearchTerm)
        {
            List<string> result = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetJobDescription(SearchTerm).ToList<string>();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult getHash(string hash)
        {
            List<GetHashInfo_Result> result = new List<GetHashInfo_Result>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetHashInfo(hash).ToList<GetHashInfo_Result>();
            }
            result.Reverse();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public List<string> getRoles()
        {
            List<string> result = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetRole(null).ToList<string>();
            }

            return result;
        }

        [HttpGet]
        public string AssignIndividualsToUser(string UserName, string Hash)
        {
            //List<string> result = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                //PMSPEntities.UserAss(Hash, UserName,"UserToIndividual");
            }

            return "Success";
        }

        [HttpGet]
        public string DeleteGroup(string groupname)
        {
            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                PMSPEntities.GroupDelete(groupname);
            }

            return "Success";
        }

        [HttpGet]
        public string DeleteAction(string UserName, string ActionName, string From, string Description, string ForwardToUser, string Notify)
        {
            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                PMSPEntities.UserByActionDelete(UserName, ActionName, Description, null, ForwardToUser, From, Notify);
            }

            return "Success";
        }


        [HttpGet]
        public string OwnerFieldAssign(string userName, string ownerColumn)
        {
            //List<string> result = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                PMSPEntities.OwnerFieldsAss(ownerColumn , userName);
            }

            return "Success";
        }


        [HttpGet]
        public JsonResult CreateGroup(string createdBy, string GroupName, string Hash, string assignedToUser, string groupDescription = null)
        {
            try
            {
                using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
                {
                    List<GetGroupsAndUsers_Result> result = new List<GetGroupsAndUsers_Result>();


                    result = PMSPEntities.GetGroupsAndUsers(null, null).ToList<GetGroupsAndUsers_Result>();

                    GetGroupsAndUsers_Result groupNameExist = new GetGroupsAndUsers_Result();
                    groupNameExist = result.Where(x => x.Group == GroupName).FirstOrDefault();
                    if (groupNameExist != null)
                    {
                        return Json("Warning Group Name Exists!", JsonRequestBehavior.AllowGet);
                    }
                    Hash = Hash.Replace("_", " ");
                    if(!Hash.Contains("N''"))
                    {
                        Hash = Hash.Replace("'", "''");
                    }
                    PMSPEntities.UserAss(Hash, assignedToUser, "UserToIndividual", GroupName, groupDescription, 0, createdBy);
                    
                }
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }




          public string EditGroup(string oldgroupname, string newgroupname, string newgroupdesc)
        {          
                using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
                {
                List<GetGroupsAndUsers_Result> result = new List<GetGroupsAndUsers_Result>();

                result = PMSPEntities.GetGroupsAndUsers(null, null).ToList<GetGroupsAndUsers_Result>();
                if (oldgroupname != newgroupname)
                {
                    GetGroupsAndUsers_Result groupNameExist = new GetGroupsAndUsers_Result();
                    groupNameExist = result.Where(x => x.Group == newgroupname).FirstOrDefault();
                    if (groupNameExist != null)
                    {
                        
                        return ("Warning Group Name Exists!");

                    }
                }
                   

                PMSPEntities.GroupUpdate(newgroupname, newgroupdesc, oldgroupname,"");
                }
            return "Success";

        }

        [HttpGet]
        public JsonResult getServicesJSON(string service = null)
        {
            List<string> result = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetServices(service).ToList<string>();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
            //return result;
        }


        [HttpGet]
        public List<string> getMohafaza()
        {
            List<string> result = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetMohafaza(null).ToList<string>();
            }

            return result;
        }

        [HttpGet]
        public List<string> getKadaa()
        {
            List<string> result = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetKadaa(null).ToList<string>();
            }

            return result;
        }



        [HttpGet]
        public List<string> getServices(string service = null)
        {
            List<string> result = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetServices(null).ToList<string>();
            }
            
            return result;
        }
        

        //[HttpGet]
        //public List<string> getBalda()
        //{
        //    List<string> result = new List<string>();

        //    using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
        //    {
        //        result = PMSPEntities.GETBalda(null).ToList<string>();
        //    }

        //    return result;
        //}


        [HttpGet]
        public JsonResult getBalda(string id, string SearchTerm)
        {
            List<string> result = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GETBalda(SearchTerm).ToList<string>();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public List<string> getNeighborhood()
        {
            List<string> neighborhood = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                //neighborhood = PMSPEntities.GETBalda(null).ToList<string>();
                neighborhood = PMSPEntities.GetNeighborhood(null).ToList<string>();
            }

            if(neighborhood.Count == 1 && neighborhood[0] == null)
            {
                neighborhood[0] = "beirut";
            }

            return neighborhood;
        }

        [HttpPost]
        public JsonResult getIndividuals(string hash = "1=1", string pagination = "")
        {
            //Edit By Hussein
            List<GetIndividuals_Result> result = new List<GetIndividuals_Result>();

            hash = hash.Replace(@"''", "'");
            hash = hash.Replace("'''", "''");
            hash = hash.Replace("_", " ");
            hash = hash.Replace("or [بحاجة لنقل] <> ''", "");
            hash = hash.Replace("or [انتخب 2009] <> ''", "");
            
            if (hash != "1=1")
            {
                hash = "1=1" + hash;
            }
            if (pagination == "")
            {
                pagination = "RN BETWEEN 1 AND 1000";
            }

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetIndividuals(hash, pagination,"1").ToList<GetIndividuals_Result>().Take(1000).ToList();
            }

            result.ForEach(x => x.CheckBox = "");

            Dictionary<string, List<GetIndividuals_Result>> dictionnaryResult = new Dictionary<string, List<GetIndividuals_Result>>();

            dictionnaryResult.Add("data", result);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult getUsers()
        {
            List<GetUsers_Result> result = new List<GetUsers_Result>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetUsers("1 = 1").ToList<GetUsers_Result>();
            }

            result.ForEach(x => x.CheckBox = "");

            Dictionary<string, List<GetUsers_Result>> dictionnaryResult = new Dictionary<string, List<GetUsers_Result>>();

            dictionnaryResult.Add("data", result);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult getActions()
        {
            List<Object> result = new List<object>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GETActions("").ToList<object>();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getFromIndividuals(string selectVariable = null, string whereVariable = null, string resultVariable = null)
        {
            whereVariable = whereVariable == "" ? null : whereVariable.Replace("_", " ");
            resultVariable = resultVariable == "" ? null : resultVariable.Replace("_", " ");
            selectVariable = selectVariable == "" ? null : selectVariable.Replace("_", " ");
            List<string> result = new List<string>();
            List<string> result2 = new List<string>();
            List<GetFromIndividualModel> items = new List<GetFromIndividualModel>();
            //selectVariable = selectVariable.Replace("_", " ");
            //whereVariable = whereVariable.Replace("_", " ");
            //resultVariable = resultVariable.Replace("_", " ");

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                try
                {
                    result = PMSPEntities.GetFromIndividuals(whereVariable, selectVariable, resultVariable).ToList<string>();
                }
                catch
                {

                }
            }
            if (selectVariable == "جمعية 1")
            {
                using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
                {
                    try
                    {
                        var newResult = PMSPEntities.GetFromIndividuals(null, "جمعية 2", null).ToList<string>();
                        var combinedResult = result.Union(newResult).ToList();
                        result = combinedResult;
                    }
                    catch
                    {
                        result.Add("");
                    }



                }

            }

            if (selectVariable == "جمعية 2")
            {
                using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
                {
                    try
                    {
                        var newResult = PMSPEntities.GetFromIndividuals(null, "جمعية 1", null).ToList<string>();
                        var combinedResult = result.Union(newResult).ToList();
                        result = combinedResult;
                    }
                    catch
                    {
                        result.Add("");
                    }



                }
            }

            result.RemoveAll(x => x == "NA");
            result.RemoveAll(x => x.Length == 0);
            result.Add(string.Empty);

            result = result.OrderBy(x => x).ToList();

            result.Where(x => x == "NA").Select(x => x).ToList().ForEach(x => x = "");

            foreach (var myItem in result)
            {
                GetFromIndividualModel item = new GetFromIndividualModel();
                item.Name = myItem;
                item.Value = myItem;
                items.Add(item);
            }
            items = items.OrderBy(x => x.Value).ToList();
            if (selectVariable == "قضاء النفوس")
            {
                items = items.Where(x => x.Value == "بيروت الأولى").ToList();
            }

            return Json(items.Select(x => x.Value).ToList<string>(), JsonRequestBehavior.AllowGet);
        }


       
        [HttpGet]
        public JsonResult getFromIndividualsCombined(string Kadaa = null, string balda = null, string tayfe = null)
        {

            List<string> result = new List<string>();
            List<GetFromIndividualModel> items = new List<GetFromIndividualModel>();
            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {

                if (balda == null || balda == "")
                {
                    result = PMSPEntities.Database.SqlQuery<string>("SELECT [رقم السجل] FROM [BeirutUI].[dbo].[Individual] where[قضاء النفوس] = N'" + Kadaa + "'  AND [الطائفة] = N'" + tayfe + "' group by [رقم السجل]").ToList<string>();
                }
                else
                {
                    result = PMSPEntities.Database.SqlQuery<string>("SELECT [رقم السجل] FROM [BeirutUI].[dbo].[Individual] where[قضاء النفوس] = N'" + Kadaa + "' AND [بلدة النفوس] = N'" + balda + "' AND [الطائفة] = N'" + tayfe + "' group by [رقم السجل]").ToList<string>();
                }

            }

            foreach (var myItem in result)
            {
                GetFromIndividualModel item = new GetFromIndividualModel();
                item.Name = myItem;
                item.Value = myItem;
                items.Add(item);
            }
            items = items.OrderBy(x => x.Value).ToList();

            return Json(items.Select(x => x.Value).ToList<string>(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getGroupsAndUsers(string user = null, string group = null)
        {
            //user = user == "" ? null : user;
            if (user == "")
                user = null;
            if (group == "")
                group = null;

            List<GetGroupsAndUsers_Result> result = new List<GetGroupsAndUsers_Result>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetGroupsAndUsers(user, group).ToList<GetGroupsAndUsers_Result>();
            }

            result.ForEach(x => x.CheckBox = "");

            Dictionary<string, List<GetGroupsAndUsers_Result>> dictionnaryResult = new Dictionary<string, List<GetGroupsAndUsers_Result>>();

            dictionnaryResult.Add("data", result);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getDataOwners()
        {
            List<GETOwnerFields_Result> result = new List<GETOwnerFields_Result>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GETOwnerFields(null).ToList<GETOwnerFields_Result>();
            }

            result.ForEach(x => x.CheckBox = "");

            Dictionary<string, List<GETOwnerFields_Result>> dictionnaryResult = new Dictionary<string, List<GETOwnerFields_Result>>();

            dictionnaryResult.Add("data", result);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public int gettotalnumberofrows(string HashCode = null)
        {
            var individualnumber = 1;
            HashCode = HashCode.Replace("_", " ");
            HashCode = HashCode.Replace(@"''", "'");
            HashCode = HashCode.Replace("'''", "''");
            HashCode = HashCode == "" ? "1=1" : HashCode;
            HashCode = HashCode == "1=1" ? HashCode : HashCode.Substring(4);

            HashCode = HashCode.Replace("or [بحاجة لنقل] <> ''", "");
            HashCode = HashCode.Replace("or [انتخب 2009] <> ''", "");

            //HashCode = "(1=1) " + HashCode; 
            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                individualnumber = Convert.ToInt32(PMSPEntities.CountIndividuals(HashCode).ToList()[0]);
            }

            return individualnumber;
        }

        [HttpGet]
        public string setActionsForUser(string UserName, string ActionName, string ForwardToUser, string From, string Notify, string Description = null, string UserRegion = null, string VisitType = null)
        //(string user, string action, string description, string userRegion, string forwardToUser, string fromUser, string notifyUser)
        {
            UserName = UserName == "" ? null : UserName;
            ActionName = ActionName == "" ? null : ActionName;
            ForwardToUser = ForwardToUser == "" ? null : ForwardToUser;
            From = From == "" ? null : From;
            Notify = Notify == "" ? null : Notify;
            if(ActionName == "Individual Management")
            {
                Description = Description == "" ? null : Description;
            }
            else
            {
                Description = VisitType == "" ? null : VisitType;
            }
            
            UserRegion = UserRegion == "" ? null : UserRegion;
            


            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                PMSPEntities.UserByActionAssgt(UserName, ActionName, Description, UserRegion, ForwardToUser, From, Notify);
            }

            return "success";
        }

        [HttpGet]
        public string updateActionsForUser(string UserName, string ActionName, string ForwardToUser, string FromUser = null,string VisitType=null,string Description = "")
        //(string user, string action, string description, string userRegion, string forwardToUser, string fromUser, string notifyUser)
        {
            UserName = UserName == "" ? null : UserName;
            ActionName = ActionName == "" ? null : ActionName;
            ForwardToUser = ForwardToUser == "" ? null : ForwardToUser;

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                PMSPEntities.UpdateUserByAction(UserName, ActionName, ForwardToUser, FromUser, Description);
            }

            return "success";
        }


        [HttpGet]
        public JsonResult getActionsByUsers()
        {
            List<GetActionByUsers_Result> result = new List<GetActionByUsers_Result>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
            }

            Dictionary<string, List<GetActionByUsers_Result>> dictionnaryResult = new Dictionary<string, List<GetActionByUsers_Result>>();

            dictionnaryResult.Add("data", result);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult getActionsByUsersForward()
        {
            List<GetActionByUsers_Result> result = new List<GetActionByUsers_Result>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                result = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
            }

            result = result.Where(x => x.ForwardToUser != null).ToList<GetActionByUsers_Result>();

            Dictionary<string, List<GetActionByUsers_Result>> dictionnaryResult = new Dictionary<string, List<GetActionByUsers_Result>>();

            dictionnaryResult.Add("data", result);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        //async public Task<JsonResult>
        async public Task<JsonResult> RegisterUser(string UserName, string Password, string FirstName = null, string LastName = null, 
            string PhoneNumber = null, string Address = null, string JobDescription = null, string UserType = null, string Kadaa = null, 
            string Mouhafaza = null, string Balda = null, string Notes = null, string Role = null,string Dashboard =null,string LoggedInUser =null)
        {
            string jResult = "";
            string result = "";
            using (HttpClient pmClient = new HttpClient())
            {
                pmClient.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                pmClient.DefaultRequestHeaders.Accept.Clear();
                pmClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("Account/Register/");
                stringBuilder.Append("?");
                stringBuilder.AppendFormat("UserName={0}", UserName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Password={0}", Password);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("FirstName={0}", FirstName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("LastName={0}", LastName);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("PhoneNumber={0}", PhoneNumber);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Address={0}", Address);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("JobDescription={0}", JobDescription);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("UserType={0}", UserType);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Kadaa={0}", Kadaa);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Mouhafaza={0}", Mouhafaza);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Balda={0}", Balda);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Notes={0}", Notes);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Role={0}", Role);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("Dashboard={0}", Dashboard);
                stringBuilder.Append("&");
                stringBuilder.AppendFormat("LoggedInUser={0}", LoggedInUser);
                HttpResponseMessage response = await pmClient.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
                jResult = JsonConvert.SerializeObject(result);
            }
            return Json(jResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string RenderRazorViewToString(string viewName = null, Models.RegisterViewModel model = null)
        {
            //Razor Requirements for Page Load
            switch (viewName)
            {
                case "CreateUserStepOne":
                    List<string> resultRoles = getRoles();
                    try {
                        ViewData["Roles"] = resultRoles.Select(tier => new SelectListItem { Text = Convert.ToString(tier).Trim(), Value = Convert.ToString(tier).Trim() }).ToList();
                    }
                    catch {
                        ViewData["Roles"] = resultRoles.Select(tier => new SelectListItem { Text = string.Empty, Value = string.Empty }).ToList();
                    }

                    List<string> resultMohafaza = getMohafaza();
                    try
                    {
                        ViewData["Mohafaza"] = resultMohafaza.Select(tier => new SelectListItem { Text = Convert.ToString(tier).Trim(), Value = Convert.ToString(tier).Trim() }).ToList();
                    }
                    catch {
                        ViewData["Mohafaza"] = resultMohafaza.Select(tier => new SelectListItem { Text = string.Empty, Value = string.Empty }).ToList();
                    }

                    List<string> resultKadaa = getKadaa();
                    try
                    {
                        ViewData["Kadaa"] = resultKadaa.Select(tier => new SelectListItem { Text = Convert.ToString(tier).Trim(), Value = Convert.ToString(tier).Trim() }).ToList();
                    }
                    catch {
                        ViewData["Kadaa"] = resultKadaa.Select(tier => new SelectListItem { Text = string.Empty, Value = string.Empty }).ToList();
                    }

                    //List<string> resultBalda = getBalda();
                    //ViewData["Balda"] = resultBalda.Select(tier => new SelectListItem { Text = Convert.ToString(tier).Trim(), Value = Convert.ToString(tier).Trim() }).ToList();

                    List<string> resultneighborhood = getNeighborhood();
                    try
                    {
                        ViewData["neighborhood"] = resultneighborhood.Select(tier => new SelectListItem { Text = Convert.ToString((tier ?? "null")).Trim(), Value = Convert.ToString((tier ?? "null")).Trim() }).ToList(); //Edit By Hussein
                    }
                    catch
                    {
                        ViewData["neighborhood"] = resultneighborhood.Select(tier => new SelectListItem { Text = string.Empty, Value = string.Empty }).ToList(); //Edit By Hussein
                    }

                    ViewBag.Services = getServices(null);

                    var directorRole = RoleManager.FindByName("Director").Id;
                    var managerRole = RoleManager.FindByName("Manager").Id;

                    var Directors = UserManager.Users.Where(u => u.Roles.Any(r => r.RoleId == directorRole)).ToList();
                    var Managers = UserManager.Users.Where(u => u.Roles.Any(r => r.RoleId == managerRole)).ToList();
                    List<IdentityUser> allManagerUsers = new List<IdentityUser>();
                    allManagerUsers.AddRange(Directors);
                    allManagerUsers.AddRange(Managers);
                    List<SelectListItem> ActionUsers = allManagerUsers.Select(x => new SelectListItem { Text = x.UserName, Value = x.Id }).ToList();
                    ViewData["ActionUsers"] = ActionUsers;

                    var users = UserManager.Users.ToList();

                    List<SelectListItem> AllUsers = users.Select(x => new SelectListItem { Text = x.UserName, Value = x.Id }).ToList();
                    ViewData["AllUsers"] = AllUsers;

                    //List<SelectListItem> AllUsers = new List<SelectListItem>();
                    //AllUsers.Add(new SelectListItem() { Text = "Northern Cape", Value = "NC" });

                    ViewData["AllUsers"] = AllUsers;

                    break;
                //case "CreateUserStepTwo":
                //    var directorRole = RoleManager.FindByName("Director").Id;
                //    var managerRole = RoleManager.FindByName("Manager").Id;

                //    var Directors = UserManager.Users.Where(u => u.Roles.Any(r => r.RoleId == directorRole)).ToList();
                //    var Managers = UserManager.Users.Where(u => u.Roles.Any(r => r.RoleId == managerRole)).ToList();
                //    List<IdentityUser> allManagerUsers = new List<IdentityUser>();
                //    allManagerUsers.AddRange(Directors);
                //    allManagerUsers.AddRange(Managers);
                //    List<SelectListItem> ActionUsers = allManagerUsers.Select(x => new SelectListItem { Text = x.UserName, Value = x.Id }).ToList();
                //    ViewData["ActionUsers"] = ActionUsers;

                //    var users = UserManager.Users.ToList();

                //    List<SelectListItem> AllUsers = users.Select(x => new SelectListItem { Text = x.UserName, Value = x.Id }).ToList();
                //    ViewData["AllUsers"] = AllUsers;
                //    break;
                default:
                    break;

            }

            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }



        [HttpGet]
        public string DeleteUser(string UserName)
        {


            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                PMSPEntities.UserDelete(UserName);

            }
            return "Success";

        }

        [HttpGet]
        public string DeleteDataOwner(string username,string group, string fields)
        {


            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                PMSPEntities.OwnerFieldDelete(group, fields, username);

            }
            return "Success";

        }


        [HttpGet]
        public string GetUserRole(string Username)
        {

            List <GETUsersRole_Result> result = new List<GETUsersRole_Result>();
            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                //result = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
                result = PMSPEntities.GETUsersRole(Username).ToList<GETUsersRole_Result>(); ;

            }

          


                //string name = (from GETUsersRole_Result r in  result where r.Name==""  select r.Name).FirstOrDefault();
                //return result.Where(x=>x.Name=="").Select(x => x.Name).FirstOrDefault();
                return result[0].Name;
        }

        [HttpGet]
        public JsonResult GetAllKadaa(string selectVariable = null, string whereVariable = null, string resultVariable = null)
        {
            List<string> resultKadaa = new List<string>();

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                resultKadaa = PMSPEntities.GetKadaa(null).ToList<string>();
            }

            return Json(resultKadaa, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GETNeighborhoods(string balda = null)
        {

            List<Object> result = new List<object>();

            //selectVariable = selectVariable.Replace("_", " ");
            //whereVariable = whereVariable.Replace("_", " ");
            //resultVariable = resultVariable.Replace("_", " ");

            using (WBI_PM_DEVEntities PMSPEntities = new WBI_PM_DEVEntities())
            {
                try
                {
                    result = PMSPEntities.GetNeighborhood(balda).ToList<object>();
                }
                catch
                {
                    result.Add("");
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }

    public class FakeController : ControllerBase { protected override void ExecuteCore() { } }




}
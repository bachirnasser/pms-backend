﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS.DataInterface.EDMX;

namespace PMS.DataInterface.Controllers
{
    public class meftehsController : Controller
    {
        private Beirut1_WBEntities db = new Beirut1_WBEntities();

        // GET: meftehs
        public ActionResult Index()
        {
            return View(db.meftehs.ToList());
        }

        // GET: meftehs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mefteh mefteh = db.meftehs.Find(id);
            if (mefteh == null)
            {
                return HttpNotFound();
            }
            return View(mefteh);
        }

        // GET: meftehs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: meftehs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "mefteh_code,mefteh_name,meftef_phone")] mefteh mefteh)
        {
            if (ModelState.IsValid)
            {
                db.meftehs.Add(mefteh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mefteh);
        }

        // GET: meftehs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mefteh mefteh = db.meftehs.Find(id);
            if (mefteh == null)
            {
                return HttpNotFound();
            }
            return View(mefteh);
        }

        // POST: meftehs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "mefteh_code,mefteh_name,meftef_phone")] mefteh mefteh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mefteh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mefteh);
        }

        // GET: meftehs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mefteh mefteh = db.meftehs.Find(id);
            if (mefteh == null)
            {
                return HttpNotFound();
            }
            return View(mefteh);
        }

        // POST: meftehs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            mefteh mefteh = db.meftehs.Find(id);
            db.meftehs.Remove(mefteh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

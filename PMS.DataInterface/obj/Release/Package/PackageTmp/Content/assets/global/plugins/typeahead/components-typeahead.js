﻿var ComponentsTypeahead = function () {

    var handleTwitterTypeahead = function () {

        // Example #1
        // instantiate the bloodhound suggestion engine
        var numbers = new Bloodhound({
            datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.num); },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: [
              { num: 'user 2' },
              { num: 'mark anthony' },
              { num: 'user 10' }
            ]
        });

        // initialize the bloodhound suggestion engine
        numbers.initialize();

        // instantiate the typeahead UI
        if (App.isRTL()) {
            $('#typeahead_example_1').attr("dir", "rtl");
        }
        $('#typeahead_example_1').typeahead(null, {
            displayKey: 'num',
            hint: (App.isRTL() ? false : true),
            source: numbers.ttAdapter()
        });

    }

    return {

        init: function () {
            handleTwitterTypeahead();
        }
    };

}();

jQuery(document).ready(function () {
    ComponentsTypeahead.init();
});
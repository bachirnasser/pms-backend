﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PMS.DataInterface.Startup))]
namespace PMS.DataInterface
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

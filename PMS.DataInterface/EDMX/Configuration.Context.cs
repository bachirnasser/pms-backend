﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMS.DataInterface.EDMX
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Beirut1_WBEntities : DbContext
    {
        public Beirut1_WBEntities()
            : base("name=Beirut1_WBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<country> countries { get; set; }
        public virtual DbSet<dtproperty> dtproperties { get; set; }
        public virtual DbSet<election> elections { get; set; }
        public virtual DbSet<elections_info> elections_info { get; set; }
        public virtual DbSet<Evaluation> Evaluations { get; set; }
        public virtual DbSet<kadaa> kadaas { get; set; }
        public virtual DbSet<mefteh> meftehs { get; set; }
        public virtual DbSet<mounakheb> mounakhebs { get; set; }
        public virtual DbSet<neighborhood> neighborhoods { get; set; }
        public virtual DbSet<opinion_leaders> opinion_leaders { get; set; }
        public virtual DbSet<party> parties { get; set; }
        public virtual DbSet<profession> professions { get; set; }
        public virtual DbSet<revenue> revenues { get; set; }
        public virtual DbSet<rite> rites { get; set; }
        public virtual DbSet<service_contact> service_contact { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<balda> baldas { get; set; }
        public virtual DbSet<service_category> service_category { get; set; }
        public virtual DbSet<service_comm> service_comm { get; set; }
        public virtual DbSet<service_field> service_field { get; set; }
        public virtual DbSet<service_third_party> service_third_party { get; set; }
        public virtual DbSet<service_type> service_type { get; set; }
        public virtual DbSet<service_contacts> service_contacts { get; set; }
        public virtual DbSet<etat> etats { get; set; }
    }
}

﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMS.DataInterface.EDMX
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class PMCallCenterEntities : DbContext
    {
        public PMCallCenterEntities()
            : base("name=PMCallCenterEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Answer> Answers { get; set; }
        public virtual DbSet<AnswerTemplate> AnswerTemplates { get; set; }
        public virtual DbSet<CallStatu> CallStatus { get; set; }
        public virtual DbSet<FAQ> FAQs { get; set; }
        public virtual DbSet<GroupExtension> GroupExtensions { get; set; }
        public virtual DbSet<IndividualExtension> IndividualExtensions { get; set; }
        public virtual DbSet<Operation> Operations { get; set; }
        public virtual DbSet<OperationStatu> OperationStatus { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Script> Scripts { get; set; }
        public virtual DbSet<Survey> Surveys { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<UserByOperation> UserByOperations { get; set; }
        public virtual DbSet<UserExtension> UserExtensions { get; set; }
        public virtual DbSet<UserOperationGroup> UserOperationGroups { get; set; }
    }
}

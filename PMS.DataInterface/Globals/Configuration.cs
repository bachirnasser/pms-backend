﻿//#define PRODUCTION

using System;
using System.Collections.Generic;
using System.Web;


namespace PMS.DataInterface.Globals
{
    public static class Configuration
    {
        public static string APPLICATION_TITLE = "People Management System";
        public static string APPLICATION_CLIENT = "PMS";

        public static string APPLICATION_FILE_UPLOAD_STORAGE_PATH = @"C:\Temp";

#if PRODUCTION
        public static string DATA_INTERFACE_URL = "http://localhost:1605/";

#else
        public static string DATA_INTERFACE_URL = "http://localhost:1684/";
        //public static string DATA_INTERFACE_URL = "http://localhost/PMS.DataInterface/";
#endif

    }
}